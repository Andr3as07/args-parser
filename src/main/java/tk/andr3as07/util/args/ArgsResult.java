package tk.andr3as07.util.args;

import java.util.*;

/**
 * Represents the result of a Parsed Argument List
 *
 * @author Andr3as07
 * @since args 0.0.1
 */
public class ArgsResult {

	final Map<String, String> strings;
	final List<String> booleans;

	/**
	 * Creates a new Args Result
	 *
	 * @since args 0.0.1
	 */
	public ArgsResult() {
		this.strings = new HashMap<String, String>();
		this.booleans = new ArrayList<String>();
	}

	/**
	 * Gets whether the result contains the Argument
	 *
	 * @since args 0.0.1
	 * @param name
	 *            the name of the Argument
	 * @return true if the argument is present
	 */
	public boolean containsBoolean(final String name) {
		return this.booleans.contains(name);
	}

	/**
	 * Gets whether the result contains the Argument
	 *
	 * @since args 0.0.1
	 * @param name
	 *            the name of the Argument
	 * @return true if the argument is present
	 */
	public boolean containsString(final String name) {
		return this.strings.containsKey(name);
	}

	/**
	 * Gets the Boolean value from the Argument if it is not present it returns
	 * false
	 *
	 * @since args 0.0.1
	 * @param name
	 *            the name of the Argument
	 * @return the value of the Argument
	 */
	public boolean getBoolean(final String name) {
		return this.containsBoolean(name) ? true : false;
	}

	/**
	 * Gets the Byte value from the Argument if it is not present or Malformed
	 * it throws a NumberFormatException
	 *
	 * @since args 0.0.3
	 * @param name
	 *            the name of the Argument
	 * @return the value of the Argument
	 * @throws NumberFormatException
	 *             if the string does not contain a parsable Byte.
	 */
	public byte getByte(final String name) throws NumberFormatException {
		try {
			return Byte.parseByte(this.getString(name));
		} catch (final NumberFormatException ex) {
			throw ex;
		}
	}

	/**
	 * Gets the Double value from the Argument if it is not present or Malformed
	 * it throws a NumberFormatException
	 *
	 * @since args 0.0.3
	 * @param name
	 *            the name of the Argument
	 * @return the value of the Argument
	 * @throws NumberFormatException
	 *             if the string does not contain a parsable Double.
	 */
	public double getDouble(final String name) throws NumberFormatException {
		try {
			return Double.parseDouble(this.getString(name));
		} catch (final NumberFormatException ex) {
			throw ex;
		}
	}

	/**
	 * Gets the Float value from the Argument if it is not present or Malformed
	 * it throws a NumberFormatException
	 *
	 * @since args 0.0.3
	 * @param name
	 *            the name of the Argument
	 * @return the value of the Argument
	 * @throws NumberFormatException
	 *             if the string does not contain a parsable Float.
	 */
	public float getFloat(final String name) throws NumberFormatException {
		try {
			return Float.parseFloat(this.getString(name));
		} catch (final NumberFormatException ex) {
			throw ex;
		}
	}

	/**
	 * Gets the Integer value from the Argument if it is not present or
	 * Malformed it throws a NumberFormatException
	 *
	 * @since args 0.0.3
	 * @param name
	 *            the name of the Argument
	 * @return the value of the Argument
	 * @throws NumberFormatException
	 *             if the string does not contain a parsable integer.
	 */
	public int getInteger(final String name) throws NumberFormatException {
		try {
			return Integer.parseInt(this.getString(name));
		} catch (final NumberFormatException ex) {
			throw ex;
		}
	}

	/**
	 * Gets the Long value from the Argument if it is not present or Malformed
	 * it throws a NumberFormatException
	 *
	 * @since args 0.0.3
	 * @param name
	 *            the name of the Argument
	 * @return the value of the Argument
	 * @throws NumberFormatException
	 *             if the string does not contain a parsable Long.
	 */
	public long getLong(final String name) throws NumberFormatException {
		try {
			return Long.parseLong(this.getString(name));
		} catch (final NumberFormatException ex) {
			throw ex;
		}
	}

	/**
	 * Gets the Short value from the Argument if it is not present or Malformed
	 * it throws a NumberFormatException
	 *
	 * @since args 0.0.3
	 * @param name
	 *            the name of the Argument
	 * @return the value of the Argument
	 * @throws NumberFormatException
	 *             if the string does not contain a parsable Short.
	 */
	public short getShort(final String name) throws NumberFormatException {
		try {
			return Short.parseShort(this.getString(name));
		} catch (final NumberFormatException ex) {
			throw ex;
		}
	}

	/**
	 * Gets the String value from the Argument if it is not present it returns
	 * an empty String
	 *
	 * @since args 0.0.1
	 * @param name
	 *            the name of the Argument
	 * @return the value of the Argument
	 */
	public String getString(final String name) {
		return this.containsString(name) ? this.strings.get(name) : "";
	}
}