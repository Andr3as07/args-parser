package tk.andr3as07.util.args;

import java.util.*;

/**
 * Utility Class to parse Command line arguments
 *
 * @author Andr3as07
 * @since args 0.0.1
 */
public class ArgsParser {

	/**
	 * Gets a String of Characters to represent a Space.
	 *
	 * @since args 0.0.2
	 * @param input the String in which the replacement is not to find
	 * @return the replacement
	 */
	private static String getSpaceReplacementString(final String input) {
		String replacer = "soTotallyNotASpaceIPromise";
		while (input.contains(replacer))
			replacer = Long.toString(Math.abs(new Random().nextLong()), Character.MAX_RADIX);
		return replacer;
	}

	/**
	 * Splits the input String into a String[] using the UNIX Standard of Space
	 * Escaping.
	 *
	 * @since args 0.0.2
	 * @param input
	 *            the String to Split
	 * @return the resulting String[]
	 */
	private static String[] split(String input) {
		final String spaceReplacerString = getSpaceReplacementString(input);
		input = input.replaceAll("\\\\\\s", spaceReplacerString);

		final String[] split = input.split(" ");
		for (int i = 0; i < split.length; i++)
			split[i] = split[i].replaceAll(spaceReplacerString, " ");
		return split;
	}

	private final Map<String, Boolean> args;

	private final Map<Character, String> alias;

	private final Map<String, String> def;

	/**
	 * Creates a new Args Parser
	 *
	 * @since args 0.0.1
	 */
	public ArgsParser() {
		this.args = new HashMap<String, Boolean>();
		this.alias = new HashMap<Character, String>();
		this.def = new HashMap<String, String>();
	}

	/**
	 * Adds an alias to an existing Argument
	 *
	 * @since args 0.0.1
	 * @param name
	 *            the name of the Argument
	 * @param alias
	 *            the new Alias to set
	 * @return true when the action was successful
	 */
	public boolean addAlias(final String name, final char alias) {
		if (!this.containsArgument(name) || this.containsAlias(alias))
			return false;
		this.alias.put(alias, name);
		return true;
	}

	/**
	 * Adds an argument to parse
	 *
	 * @since args 0.0.1
	 * @param name
	 *            the name of the new argument
	 * @param expectsValue
	 *            if the argument is represented by a String or Boolean
	 */
	public void addArgument(final String name, final boolean expectsValue) {
		if (name == null)
			return;
		this.args.put(name, expectsValue);
	}

	/**
	 * Adds an argument to parse
	 *
	 * @since args 0.0.1
	 * @param name
	 *            the name of the new argument
	 * @param expectsValue
	 *            if the argument is represented by a String or Boolean
	 * @param alias
	 *            the one Character alias for this Argument
	 */
	public void addArgument(final String name, final boolean expectsValue, final char alias) {
		this.addArgument(name, expectsValue);
		this.addAlias(name, alias);
	}

	/**
	 * Adds an argument to parse
	 *
	 * @since args 0.1.0
	 * @param name
	 *            the name of the new argument
	 * @param def
	 *            the default Value the String get assigned if none is found
	 */
	public void addArgument(final String name, final String def) {
		this.addArgument(name, true);
		this.setDefault(name, def);
	}

	/**
	 * Adds an argument to parse
	 *
	 * @since args 0.1.0
	 * @param name
	 *            the name of the new argument
	 * @param def
	 *            the default Value the String get assigned if none is found
	 * @param alias
	 *            the one Character alias for this Argument
	 */
	public void addArgument(final String name, final String def, final char alias) {
		this.addArgument(name, true, alias);
		this.setDefault(name, def);
	}

	/**
	 * Gets whether the Alias in question is parseable by the Parser
	 *
	 * @since args 0.0.1
	 * @param alias
	 *            the Alias in question
	 * @return true if the alias is parseable
	 */
	private boolean containsAlias(final char alias) {
		return this.alias.containsKey(alias);
	}

	/**
	 * Gets whether the Argument in question is parseable by the Parser
	 *
	 * @since args 0.0.1
	 * @param name
	 *            the name of the new argument
	 * @return true if the argument is parseable
	 */
	private boolean containsArgument(final String name) {
		if (name == null)
			return false;
		return this.args.containsKey(name);
	}

	/**
	 * Gets whether the argument is represented as a String
	 *
	 * @since args 0.0.1
	 * @param name
	 *            the name of the Argument
	 * @return true when the argument has to represents as a String
	 */
	private boolean expectsValue(final String name) {
		if (name == null)
			// Error
			return false;
		if (!this.containsArgument(name))
			// Error
			return false;
		return this.args.get(name);
	}

	/**
	 * Gets the Argument for an Alias
	 *
	 * @since args 0.0.1
	 * @param alias
	 *            the Alias in question
	 * @return the Argument of the Alias
	 */
	private String getArgument(final char alias) {
		if (!this.containsAlias(alias))
			// Error
			return null;
		return this.alias.get(alias);
	}

	/**
	 * Parses the Args String into an ArgsResult Object use UNIX Standard of
	 * Space Escaping
	 *
	 * @since args 0.0.2
	 * @param argsStr
	 *            the String that represents the command line Arguments
	 * @return the parsed result
	 */
	public ArgsResult parse(final String argsStr) {
		final String[] split = split(argsStr);
		return this.parseRaw(split);
	}

	/**
	 * Parses the Args Array into an ArgsResult Object use UNIX Standard of
	 * Space Escaping
	 *
	 * @since args 0.0.2
	 * @param args
	 *            the String[] that represents the command line Arguments
	 * @return the parsed result
	 */
	public ArgsResult parse(final String[] args) {
		String argsStr = "";
		for (final String str : args)
			argsStr += str + " ";
		return this.parse(argsStr.substring(0, argsStr.length() - 1));
	}

	/**
	 * Parses the Args Array into an ArgsResult Object
	 *
	 * @since args 0.0.1
	 * @param args
	 *            the String[] that represents the command line Arguments
	 * @return the parsed result
	 */
	private ArgsResult parseRaw(final String[] args) {
		final ArgsResult result = new ArgsResult();

		for (int i = 0; i < args.length; i++) {

			String argument = "";

			if (args[i].matches("^(-[a-zA-Z0-9])$")) {
				// Alias
				final String alias = args[i].substring(1);
				if (this.containsAlias(alias.charAt(0)))
					argument = this.getArgument(alias.charAt(0));
			} else
				argument = args[i].substring(2);

			// Argument
			if (this.containsArgument(argument))
				if (this.expectsValue(argument)) {
					if (i + 1 == args.length) {
						// Error
					} else
						result.strings.put(argument, args[i + 1]);
				} else
					result.booleans.add(argument);
		}

		for (final String name : this.def.keySet())
			if (!result.containsString(name) && this.expectsValue(name))
				result.strings.put(name, this.def.get(name));

		return result;
	}

	/**
	 * Sets the default value for an argument
	 *
	 * @since args 0.1.0
	 * @param name
	 *            the name of the new argument
	 * @param def
	 *            the default Value the String get assigned if none is found
	 * @return true if the default has been set
	 */
	public boolean setDefault(final String name, final String def) {
		if (!this.containsArgument(name) || !this.expectsValue(name))
			return false;
		this.def.put(name, def);
		return true;
	}
}