package tk.andr3as07.util.args;

import org.junit.Test;

import junit.framework.TestCase;

public class ArgsParserTest extends TestCase {

	@Test
	public static void testParser() {
		ArgsParser parser = new ArgsParser();
		parser.addArgument("str1", true);
		parser.addArgument("bool1", false);
		parser.addArgument("str2", true, 's');
		parser.addArgument("bool2", false, 'b');
		parser.addArgument("str3", "This is a Test");
		parser.addArgument("str4", "This is an other Test", 't');

		parser.addArgument("str5", true);
		parser.addArgument("str6", "Foo");
		parser.addArgument("bool3", false);

		parser.addArgument("str7", true);
		parser.addArgument("str8", true);

		parser.addArgument("byte", "0", 'b');
		parser.addArgument("short", "0");
		parser.addArgument("integer", "0", 'i');
		parser.addArgument("long", "0", 'l');
		parser.addArgument("float", "0.0", 'f');
		parser.addArgument("double", "0.0", 'd');

		ArgsResult result = parser.parse("--str1 my\\ name\\ is\\ Andr3as07 --bool1 -s Some\\ other\\ Text -b --str3 I\\ need\\ something\\ to\\ write\\ here -i 10 --byte 10 --short 329 -l 5040 -f 17.17 -d -4.3332 --str7 This\\ String\\ contains\\ two\\ \\\\\\ isn't\\ that\\ crazy --str8 soTotallyNotASpaceIPromise");

		assertEquals("my name is Andr3as07", result.getString("str1"));
		assertTrue(result.getBoolean("bool1"));
		assertEquals("Some other Text", result.getString("str2"));
		assertTrue(result.getBoolean("bool2"));
		assertEquals("I need something to write here", result.getString("str3"));
		assertEquals("This is an other Test", result.getString("str4"));
		assertEquals("", result.getString("str5"));
		assertEquals("Foo", result.getString("str6"));
		assertFalse(result.getBoolean("bool3"));
		assertEquals("This String contains two \\\\ isn't that crazy", result.getString("str7"));
		assertEquals("soTotallyNotASpaceIPromise", result.getString("str8"));
		assertEquals((byte) 10, result.getByte("byte"));
		assertEquals((short) 329, result.getShort("short"));
		assertEquals(10, result.getInteger("integer"));
		assertEquals(5040l, result.getLong("long"));
		assertEquals(17.17f, result.getFloat("float"));
		assertEquals(-4.3332d, result.getDouble("double"));
	}
}